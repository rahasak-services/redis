FROM        ubuntu:16.04

MAINTAINER Eranga Bandara (erangaeb@gmail.com)

# install redis
RUN         apt-get update && apt-get install -y redis-server

EXPOSE      6379

ENTRYPOINT  ["/usr/bin/redis-server"]
